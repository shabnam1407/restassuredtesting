Feature: Test to validate Create employee features

  @test
  Scenario Outline: User create an employee
    Given create a stub for create employee "<apiName>"
    Then send a create employee request "<apiName>" with the details "<id>" "<employeeName>" "<firstName>" "<lastName>" "<email>" "<password>" "<phone>" "<employeeStatus>"
    When successfully POST the status code is "<statusCode>"

    Examples:
      |apiName |statusCode|id|employeeName  |firstName|lastName|email       |password |phone  |employeeStatus|
      |employee|200       | 1|shabnamfathima|shabnam  |fathima |xx@gmail.com|password1|0123456|0             |

  @test
  Scenario Outline: User create a employee with a list
    Given create a stub for create employee "<apiName>"
    Then generate a list of employee request with list
      |id|employeeName   |firstName|lastName|email          |password |phone  |employeeStatus|
      | 1|shabnamfathima |shabnam  |fathima |xx@gmail.com   |password1|0123456|0             |
      | 3|DivyaRaj       |Divya    |Raj     |abc@gmail.com  |password2|9993457|1             |
      | 2|Inthazam K     |Inthazam |K       |IKxx@gmail.com |password3|0123456|1             |
    And send the POST request to "<apiName>" for Employee list
    When successfully POST the status code is "<statusCode>"

    Examples:
      |apiName |statusCode|
      |employee|200       |

  @test
  Scenario Outline: User create a employee with a Array
    Given create a stub for create employee "<apiName>"
    Then generate a list of employee request with array
      |id|employeeName   |firstName|lastName|email          |password |phone    |employeeStatus|
      | 1|shabnamfathima |shabnam  |fathima |xx@gmail.com   |password1|0123456  |0             |
      | 2|DivyaRaj       |Divya    |Raj     |abc@gmail.com  |password2|9993457  |1             |
      | 4|Inthazam K     |Inthazam |K       |IKxx@gmail.com |password3|0123456  |1             |
      | 3|SureshKumar    |Suresh   |Kumar   |SKxx@gmail.com |password4|9876540  |1             |
    And send the POST request to "<apiName>" for Employee array
    When successfully POST the status code is "<statusCode>"

    Examples:
      |apiName |statusCode|
      |employee|200       |
