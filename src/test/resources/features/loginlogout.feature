Feature: Test to validate the login and logout features

  @test
  Scenario Outline: an /employee/login api to logs employee into the system
    Given create a stub for a "login" request with "<apiName>" "<userName>" "<userPassword>" "<X-Rate-Limit>" "<X-Expires-After>"
    When a users makes a "login" request with a "<apiName>" "<userName>" "<userPassword>"
    Then the status code "<statusCode>" is returned
    And Validate the header response "<X-Rate-Limit>" "<X-Expires-After>"

  Examples:
  |apiName       |userName|userPassword|statusCode|X-Rate-Limit|X-Expires-After|
  |employee/login|user1   |password1   |200       |1           |2 Days         |
  |employee/login|user2   |password2   |200       |2           |2 Months       |

  @test
  Scenario Outline: an /employee/logout api to Logs out current logged in employee session
    Given create a stub for a "logout" request with "<apiName>" "" "" "" ""
    When a users makes a "logout" request with a "<apiName>" "" ""
    Then the status code "<statusCode>" is returned

    Examples:
      |apiName        |statusCode|
      |employee/logout|200       |
