# rest-assured-cucumber

A test project for running the rest-assured api testing using cucumber and Maven

# Instructions

Clone the repo:

Git:
```
$ git clone https://shabnam1407@bitbucket.org/shabnam1407/restassuredtesting.git
```
Or download a ZIP of master [manually](https://bitbucket.org/shabnam1407/restassuredtesting/src/master/) and expand the contents someplace on your system

## Prerequisites

In order to run the test in the local machine the below dependencies should be installed to your system.
1. Java 8
2. Maven
3. Intellij or eclipse

Note: Import/Create it as Maven project.

(TODO after testing on Windows)

## Use Maven

Open a command window and run:
    For the first run :
        
    mvn clean install
    
    mvn test

This runs Cucumber features using Cucumber's JUnit runner. The `@RunWith(Cucumber.class)` annotation on the `MyRunner`
class tells JUnit to kick off Cucumber.

##Build - Test Scenarios
Features:

Referring to the swagger "employee.yaml file", 3 features files are build covering the following scenarios:
1. createEmployee : It covers the scenarios of  POST operation  like employee creation, creation using array and creation using list.

2. loginlogout :  It covers the scenarios of  GET operation by getting the details of employee login/ logged out and also covers the negative scenario of Invalid employeename/password supplied.

3. ManageEmployee : It covers the scenarios of Get/Put/Delete operations. GET operation retrives the details of employee by employee name and covers the Invalid secnarios - invalid Employee name and employee not found. It also updates and deletes covering the operations - PUT and DELETE


## Verify installation

The test generates html reports and the report can be checked in the results folder
/results/cucumber-reports/report.html

Also you can refer the Test Result uploaded separately in Taget Folder in BitBucket master.
